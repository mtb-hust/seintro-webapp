package com.example.intro.se.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "epidemiology_report")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EpidReport {
    @Id
    @GeneratedValue
    @Column(name = "id",nullable = false)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name ="destination", nullable = false, length = 45)
    private String des;

    @Column(name ="arrive_time", nullable = false)
    private Date arrTime;

    @Column(name ="vehicle", nullable = false, length = 45)
    private String vehicle;

    @Column(name ="vehicle_code",  length = 45)
    private String vehCode;

    @Column(name ="recently_arrive", nullable = false, length = 45)
    private String recentArr;

    @Column(name ="has_fever", nullable = false)
    private boolean fever;

    @Column(name ="has_cough", nullable = false)
    private boolean cough;

    @Column(name ="is_short_winded", nullable = false)
    private boolean shortWinded;

    @Column(name ="is_skin_blooded", nullable = false)
    private boolean skinBlooded;

    @Column(name ="pharmacy")
    private String pharmacy;

}
