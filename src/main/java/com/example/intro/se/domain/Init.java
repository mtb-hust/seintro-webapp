package com.example.intro.se.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Init {
    @Id
    @GeneratedValue
    private Long id;

    @Column(length=50, nullable=false)
    private String name;

    @Column(length = 12, nullable = false)
    private String telNo;

    @Column(nullable = false)
    private Date createdOn;

    @Column(nullable = true)
    private Date modifiedOn;
}
