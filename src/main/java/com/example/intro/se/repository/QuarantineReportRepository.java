package com.example.intro.se.repository;

import com.example.intro.se.domain.QuarantineReport;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface QuarantineReportRepository extends JpaRepository<QuarantineReport, Long> {
    List<QuarantineReport> findAllByUserId(Long userId);
}
