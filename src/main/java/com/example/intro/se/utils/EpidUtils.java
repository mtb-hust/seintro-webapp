package com.example.intro.se.utils;

import com.example.intro.se.domain.EpidReport;
import com.example.intro.se.dto.request.EpidCreateRequestDTO;

public class EpidUtils {
    public static EpidReport toDAO(Long userId, EpidCreateRequestDTO createRequestDTO){
        return EpidReport.builder()
                .userId(userId)
                .des(createRequestDTO.getDes())
                .arrTime(createRequestDTO.getArrTime())
                .vehicle(createRequestDTO.getVehicle())
                .vehCode(createRequestDTO.getVehCode())
                .recentArr(createRequestDTO.getRecentArr())
                .fever(createRequestDTO.isFever())
                .cough(createRequestDTO.isCough())
                .shortWinded(createRequestDTO.isShortWinded())
                .skinBlooded(createRequestDTO.isSkinBlooded())
                .pharmacy(createRequestDTO.getPharmacy())
                .build();
    }
}
