package com.example.intro.se.utils;

import com.example.intro.se.domain.Init;
import com.example.intro.se.dto.request.InitCreateRequestDTO;
import com.example.intro.se.dto.request.InitUpdateRequestDTO;
import com.example.intro.se.dto.response.InitResponseDTO;

import java.util.Date;

public class InitUtils {
    public static InitResponseDTO toDto(Init init){
        return InitResponseDTO.builder().name(init.getName())
                .tellNo(init.getTelNo())
                .createdOn(init.getCreatedOn()).build();
    }
    public static Init toEntity(InitCreateRequestDTO requestDTO){
        return Init.builder().name(requestDTO.getName())
                .telNo(requestDTO.getTelNo())
                .createdOn(new Date())
                .build();

    }
    public static Init toEntity(Init init, InitUpdateRequestDTO initUpdateRequestDTO){
        init.setName(initUpdateRequestDTO.getName());
        init.setTelNo(initUpdateRequestDTO.getTelNo());
        init.setModifiedOn(new Date());
        return init;
    }
}
