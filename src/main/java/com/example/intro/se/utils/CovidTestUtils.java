package com.example.intro.se.utils;

import com.example.intro.se.domain.CovidTest;
import com.example.intro.se.dto.request.CovidTestCreateRequestDTO;

public class CovidTestUtils {
    public static CovidTest toEntity(Long quarantineId,int times, CovidTestCreateRequestDTO requestDTO){
        return CovidTest.builder()
                .quarantineId(quarantineId)
                .times(times)
                .kind(requestDTO.getKind())
                .result(requestDTO.getResult())
                .time(requestDTO.getTime())
                .build();
    }
}
