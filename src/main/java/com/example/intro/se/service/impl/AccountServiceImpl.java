package com.example.intro.se.service.impl;

import com.example.intro.se.domain.Account;
import com.example.intro.se.repository.AccountRepository;
import com.example.intro.se.service.AccountService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@AllArgsConstructor
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    @Override
    public String isLogin(String userName, String password) {
        Account temp = accountRepository.findAccountByUserName(userName).orElseThrow(
                () -> new EntityNotFoundException());
        return temp.getUserName();
    }
}
