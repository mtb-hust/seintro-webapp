package com.example.intro.se.service;

import com.example.intro.se.domain.CovidTest;
import com.example.intro.se.dto.request.Generic.CreateReportRequestDTO;
import com.example.intro.se.dto.request.QuarantineReportCreateRequestDTO;
import com.example.intro.se.dto.response.QuarantineResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface QuarantineService {
    QuarantineResponseDTO create(CreateReportRequestDTO<Long,QuarantineReportCreateRequestDTO>   createRequestDTO);
    Page<QuarantineResponseDTO> findAll(Pageable pageable);
    QuarantineResponseDTO getQById(Long quarantineId);
    List<CovidTest> getListCovidTestsByQId(Long quarantineId);
    List<QuarantineResponseDTO> getListQuarantineByUserId(Long userId);
}
