package com.example.intro.se.controller;

import com.example.intro.se.dto.request.UserCreateRequestDTO;
import com.example.intro.se.dto.request.UserUpdateRequestDTO;
import com.example.intro.se.dto.response.UserResponseDTO;
import com.example.intro.se.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/user")
@AllArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping()
    public ResponseEntity<Page<UserResponseDTO>> findAll(Pageable pageable){
        return new ResponseEntity<>(userService.getAll(pageable), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<UserResponseDTO> getById(@PathVariable Long id){
        return new ResponseEntity<>(userService.getById(id), HttpStatus.OK);
    }
    @PostMapping("/{id}")
    public ResponseEntity<UserResponseDTO> create(@PathVariable Long id,@RequestBody UserCreateRequestDTO createRequestDTO){
        return new ResponseEntity<>(userService.create(id, createRequestDTO), HttpStatus.OK);
    }
    @PutMapping("/{id}")
    public ResponseEntity<UserResponseDTO> update(@PathVariable Long id, @RequestBody UserUpdateRequestDTO updateRequestDTO){
        return new ResponseEntity<>(userService.update(id, updateRequestDTO), HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity update(@PathVariable Long id){
        return new ResponseEntity(HttpStatus.OK);
    }
}
