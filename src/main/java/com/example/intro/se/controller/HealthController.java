package com.example.intro.se.controller;

import com.example.intro.se.dto.request.Generic.CreateReportRequestDTO;
import com.example.intro.se.dto.request.HealthReportCreateRequestDTO;
import com.example.intro.se.dto.request.InsurranceUpdateRequestDTO;
import com.example.intro.se.dto.response.HealthResponseDTO;
import com.example.intro.se.dto.response.InsurranceUpdateResponseDTO;
import com.example.intro.se.service.HealthService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/api/v1/health")
@AllArgsConstructor
public class HealthController {

    private HealthService healthService;

    @PostMapping
    public ResponseEntity<HealthResponseDTO> create(@RequestBody CreateReportRequestDTO<Long, HealthReportCreateRequestDTO> createRequestDTO){
        return new ResponseEntity<>(healthService.create(createRequestDTO), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Page<HealthResponseDTO>> getAll(Pageable pageable){
        return new ResponseEntity<>(healthService.getAll(pageable), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<InsurranceUpdateResponseDTO> updateInsurrance(@PathVariable Long id, InsurranceUpdateRequestDTO updateRequestDTO){
        return new ResponseEntity<>(healthService.updateInsurrance(id,updateRequestDTO), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<HealthResponseDTO> getById(@PathVariable Long id){
        return new ResponseEntity<>(healthService.getById(id), HttpStatus.OK);
    }
}
