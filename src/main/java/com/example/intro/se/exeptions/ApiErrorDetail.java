package com.example.intro.se.exeptions;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel
@AllArgsConstructor
public class ApiErrorDetail {
    @ApiModelProperty(notes = "Entity name make this error", example = "Order")
    private String entityName;

    @ApiModelProperty(notes = "Field name make this error", example = "OrderId")
    private String fieldName;

    @ApiModelProperty(notes = "Field value of this error", example = "order123")
    private String fieldValue;

    @ApiModelProperty(notes = "Detailed message of this error", example = "orderId: 123 is duplicated")
    private String message;
}