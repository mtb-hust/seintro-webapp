package com.example.intro.se.exeptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class Covid19Exception extends RuntimeException {
    private final ApiError apiError;
}
