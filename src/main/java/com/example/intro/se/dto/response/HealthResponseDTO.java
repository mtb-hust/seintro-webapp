package com.example.intro.se.dto.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HealthResponseDTO {
    private Long id;

    private Long userId;

    private String insurranceId;

    private boolean fever;

    private boolean cough;

    private boolean shortWinded;

    private boolean pneumonia;

    private boolean soreThroat;

    private boolean tired;

    private boolean chronicLiver;

    private boolean chronicBlood;

    private boolean chronicLung;

    private boolean chronicKidney;

    private boolean heartRelatedDiseases;
}
