package com.example.intro.se.dto.request.Generic;

import com.example.intro.se.dto.request.Base.UserBase;

public class CreateReportRequestDTO<PKT,T> {
    public final PKT pk;
    public final UserBase user;
    public final T report;
    public CreateReportRequestDTO(){
        pk = null;
        user = null;
        report =null;
    };
    public CreateReportRequestDTO(PKT pk, UserBase user, T report) {
        this.pk = pk;
        this.user = user;
        this.report = report;
    }
}
