package com.example.intro.se.dto.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserUpdateRequestDTO {
    @NonNull
    private String address;

    @NonNull
    private String telNo;
}

