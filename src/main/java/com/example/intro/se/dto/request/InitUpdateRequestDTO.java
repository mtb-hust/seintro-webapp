package com.example.intro.se.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class InitUpdateRequestDTO {
    @NonNull
    private Long id;

    private String name;

    private String telNo;


}
