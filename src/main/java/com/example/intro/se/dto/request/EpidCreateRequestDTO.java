package com.example.intro.se.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class EpidCreateRequestDTO {
    private String des;

    @ApiModelProperty(example = "dd/MM/yyyy")
    @JsonFormat(pattern="dd/MM/yyyy")
    private Date arrTime;

    private String vehicle;

    private String vehCode;

    private String recentArr;

    private boolean fever;

    private boolean cough;

    private boolean shortWinded;

    private boolean skinBlooded;

    private String pharmacy;
}
